# Methodology of Science and Biology — Final Project
The effect of document header-body contrast on navigability

## Organization of this Repository
This repository contains all documents used for the OMSB9 final project,
these being code used for the data-gathering website, LaTeX used for the writeup
and presentation, and general documentation.

Code is contained in `src/`, where each sub-folder is a self contained module
for building, be it a Cargo crate or C(++) executable source. `src` contains a
README that further elaborates on the purposes of each sub-folder. Each
sub-folder contains both a README and an ARCHITECTURE.md which specifies the
module-level organization.

All resources intended for presentation or other non-code human reading is
contained in `pre/`, which includes the project proposal, the final writeup, and
presentation slides. These will be added when written.

This README contains (TODO) an overview of data collected and how the data is
aggregated, which is expounded upon in more detail in the corresponding
subdirectory.

## Project Overview
The goal of this project is to contribute to conventions for data-driven
document formatting, in particular by examining the effect of section
header-body text contrast on navigability. _Contrast_ is defined as the
visual difference between header text (i.e., that denoted by `#` or `##` in this
markdown document) and body text (generic text) and is measured as ratio data
defined as the Euclidean distance between text descriptor vectors.
_Navigability_ is defined as the time it takes to locate a specific header in a
given document (lower is better). More information is available in source-level
documentation and will be available in the writeup when written for submission
at the end of the semester.

### Licensing
All source, documentation, and other files are licensed under GPLv3 in the
LICENSE file.
