#!/bin/bash
# This script, intended to be run by cron, opens a connection to the database,
# and requests a write.

mkdir -p "/var/db"

echo "W" | nc -U "/tmp/msb-final-ipc-socket"
