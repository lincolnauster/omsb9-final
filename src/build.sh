#!/bin/bash

rm -rf target
mkdir target/

cargo build --bin db --release --manifest-path db/Cargo.toml --target-dir target/
mv target/release/db target/db
rm -rf target/release
