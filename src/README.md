# src
All source code used in the final project

## Frontend and Web Server
This will use Flask in Python for the web framework, and standard HTML, CSS, JS,
for the frontend. I want to use as little JS (and HTML, CSS to a lesser extent)
as is reasonably possible, so I will delegate most functionality to the Rust db
and other parsing modules at modest expense to performance.

## Backend and Database
The backend consists of a custom database framework written in rust (contained
in the `db/` subdirectory. The reasons for using a custom database over SQL or
noSQL are limited, but overall this is used for simplicity.

## Convenience
The `build.sh` script builds the binaries, `install.sh` copies them to an
appropriate directory (`/usr/local/bin` on Linux, equivalent of some sort (TODO)
on \*BSD). `run.sh` will launch the server with all necessary services, as well
as manage logging.
