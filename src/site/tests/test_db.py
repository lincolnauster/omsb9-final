""" Test that the database connection works, and that it produces expected
output."""

import sys
import os.path
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

import db
import fmt

def test_put_cc_fonts():
    a = fmt.AvailableFonts.SERIF
    b = fmt.AvailableFonts.MONOSPACE
    v = 0.7

    assert(
        'PCFserif Fmonospace 0.7'
        == str(db.PutContrastCoefficient(a, b, v)))

def test_put_cc_inlines():
    a = [fmt.AvailableInlines.BOLD, fmt.AvailableInlines.ITALIC]
    b = [fmt.AvailableInlines.UNDERLINE]
    v = 0.3

    assert(
        'PCIbold italic : Iunderline : 0.3'
         == str(db.PutContrastCoefficient(a, b, v)))

def test_put_cc_inlines_empty():
    a = []
    b = []
    v = 0.8

    assert(
        'PCI: I: 0.8'
        == str(db.PutContrastCoefficient(a, b, v)))
def test_put_cc_aligns():
    a = fmt.AvailableAligns.CENTER
    b = fmt.AvailableAligns.JUSTIFY
    v = 0.1

    assert(
        'PCAcenter Ajustify 0.1'
        == str(db.PutContrastCoefficient(a, b, v)))
