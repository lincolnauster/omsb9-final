""" Test the fmt module functions"""

import sys
import os.path
from unittest import mock

import numpy
import scipy.stats

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

import fmt

RANDOM_ITERS_MAX = 4096

test_textstyle = mock.Mock()
test_textstyle.inlines = [fmt.AvailableInlines.BOLD]
test_textstyle.align = fmt.AvailableAligns.LEFT
test_textstyle.font = fmt.AvailableFonts.SERIF
test_textstyle.size = 12

def test_as_css():
    print(fmt.text_style_as_css(test_textstyle))
    assert fmt.text_style_as_css(test_textstyle) == \
        'font-weight: bold;text-align: left;font-family: serif;'

def test_random_inlines():
    """Make sure that randomly generated inlines have an approximately equal
    chance of being selected. Uses Chi Square Goodness of Fit test."""
    inlines = [tuple(fmt.random_inlines()) for i in range(RANDOM_ITERS_MAX)]

    count = {}
    for inline in inlines:
        count[inline] = count.get(inline, 0) + 1

    vals = numpy.fromiter(count.values(), dtype=int)
    print(count)
    print(vals)
    # by default, assumed expected equally likely
    chisquare, p = scipy.stats.chisquare(vals)

    assert p > 0.05
