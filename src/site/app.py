#!/usr/bin/env python3
"""Run the app, wrapping flaks."""
import random
from uuid import uuid4
from operator import methodcaller
from flask import Flask, render_template, request, make_response

import db
import fmt
import text

app = Flask(__name__)

conn = db.conn
rtext = text.RandomText()

@app.route('/')
def index():
    """Index: simply return index.xhtml."""
    return render_template('index.xhtml')

@app.route('/contrast_coefficients', methods=['POST', 'GET'])
def contrast_coefficients():
    """CC: when a POST request is sent to the URL, the info should be inserted
    into the database. Then, a new pair is provided as if no POST request had
    been sent."""
    # insert into db: generate the put request, and fill in the err value if
    # necessary.
    if request.method == 'POST':
        inline_a = fmt.AvailableInlines.from_css(request.form['left_inline'])
        inline_b = fmt.AvailableInlines.from_css(request.form['right_inline'])
        inline_v = str(int(request.form['inlines_sim']) / 10.0)

        align_a = fmt.AvailableAligns.from_css(request.form['left_align'])
        align_b = fmt.AvailableAligns.from_css(request.form['right_align'])
        align_v = str(int(request.form['aligns_sim']) / 10.0)

        font_a = fmt.AvailableFonts.from_css(request.form['left_font'])
        font_b = fmt.AvailableFonts.from_css(request.form['right_font'])
        font_v = str(int(request.form['fonts_sim']) / 10.0)

        pcc_inline = db.PutContrastCoefficient(inline_a, inline_b, inline_v)
        pcc_align = db.PutContrastCoefficient(align_a, align_b, align_v)
        pcc_font = db.PutContrastCoefficient(font_a, font_b, font_v)

        conn.add(bytes(str(pcc_inline), 'utf-8'))
        conn.add(bytes(str(pcc_align), 'utf-8'))
        conn.add(bytes(str(pcc_font), 'utf-8'))

    # generate and return a rendered template with a pair
    one = fmt.random_text_style()
    two = fmt.random_text_style()
    return render_template('cc.xhtml',
                           lorem_ipsum=rtext.random_paragraph(),
                           left_inline=fmt.inlines_as_css(one.inlines),
                           right_inline=fmt.inlines_as_css(two.inlines),
                           left_align=fmt.align_as_css(one.align),
                           right_align=fmt.align_as_css(two.align),
                           left_font=fmt.font_as_css(one.font),
                           right_font=fmt.font_as_css(two.font))

@app.route('/aggregation')
def results():
    """Index: simply return index.xhtml."""
    return render_template('results.xhtml')

@app.route("/time_contrast")
def time_contrast(err=None):
    """This is the summary page for how timing/testing works. Actual data is
    collected separately."""
    return render_template('tc_summary.xhtml', header=rtext.random_header(), err=err)

@app.route("/tc_measure", methods=['POST'])
def time_contrast_measure(id=None):
    """This page is responsible for measuring the time it takes to locate a
    section of text. Timing information is handled client-side, but the text is
    generated server-side. If the section to locate isn't given in a POST
    request, the page redirects to /time_contrast."""
    if 'header_target' not in request.form.keys():
        return time_contrast(err='A section to load wasn\'t detected. \
                                  Try starting the test again.')
    # generate all necessary content, being styles and sections
    sections = rtext.random_text(target=request.form['header_target'])
    header_style = fmt.header_style_as_css(fmt.random_header_style())
    body_style = fmt.body_style_as_css(fmt.random_body_style())
    rq = make_response(render_template('tc_measure.xhtml',
                            sections=sections,
                            header_style=header_style,
                            body_style=body_style,
                            header_target=request.form['header_target']))

    if id != None:
        rq.set_cookie('id', id)

    return rq

@app.route('/tc_process', methods=['POST'])
def time_contrast_process():
    """Serialize the time, contrast information, submit it to the database, and
    then redirect to tc_measure."""
    id = request.cookies.get('id')
    if id == None:
        id = str(uuid4().int >> 64)

    tsr = db.PutTimeStyleRecord(
        int(id),
        request.form['ms'],
        fmt.body_style_from_css(request.form['body']),
        fmt.header_style_from_css(request.form['header'])
    )

    print(str(tsr))

    conn.add(bytes(str(tsr), 'utf-8'))

    return time_contrast_measure(id=id)

if __name__ == '__main__':
    app.run()
