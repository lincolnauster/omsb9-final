/* tc_measure.js: record timing information and submit required POST requests */

class Timing {
	constructor() {
		this._start = (new Date()).getTime();
		this._end = -1;
	}

	/* stop the timer and return the amount of time passed */
	stop() {
		this._end = (new Date()).getTime();
		return (this._end - this._start) / 1000.0;
	}
}

let time = new Timing();

/* fill out the hidden form and submit */
function submit() {
	document.getElementsByName("ms")[0].value = time.stop()
	document.getElementById("time").submit()
}

window.onload = function() { time = new Timing(); }
document.getElementById("target").onclick = submit;
