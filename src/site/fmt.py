"""The module encapsulating all formatting functions and classes, including
TextStyles, randomness, and serializing, deserializing."""
from collections import namedtuple
import enum
import re
import random
from typing import Union

class StyleType(enum.Enum):
    """An enumeration of all possible styles that may be applied to text."""
    INLINES = enum.auto
    ALIGN = enum.auto
    FONT = enum.auto
    SIZE = enum.auto

class AvailableInlines(enum.Enum):
    """An enumeration of all possible inline styles that may be applied:
    inline styles (listed below) are those that effect styling without
    affecting spacing."""
    BOLD = 'bold'
    ITALIC = 'italic'
    UNDERLINE = 'underline'

    @staticmethod
    def from_css(t: str):
        self: list[AvailableInlines] = []
        td = _css_keys_for_(t, 'text-decoration')
        if len(td) > 0:
            self.append(__class__.UNDERLINE)

        td = _css_keys_for_(t, 'font-style')
        if len(td) > 0:
            self.append(__class__.ITALIC)

        td = _css_keys_for_(t, 'font-weight')
        if len(td) > 0:
            self.append(__class__.BOLD)

        return self
    def __str__(self):
        if self == __class__.BOLD:
            return 'bold'
        if self == __class__.ITALIC:
            return 'italic'
        if self == __class__.UNDERLINE:
            return 'underline'

class AvailableAligns(enum.Enum):
    """All aligns. Importantly, no distinction is made between justification
    methods (Knuth, Greedy, etc)."""
    LEFT = 'left'
    JUSTIFY = 'justify'
    CENTER = 'center'
    RIGHT = 'right'

    @staticmethod
    def from_css(t: str):
        return __class__(_css_keys_for_(t, 'text-align')[0])

    def __str__(self):
        if self == AvailableAligns.LEFT:
            return 'left'
        if self == AvailableAligns.JUSTIFY:
            return 'justify'
        if self == AvailableAligns.CENTER:
            return 'center'
        if self == AvailableAligns.RIGHT:
            return 'right'
        # can't verify pattern matching is complete at compile time because
        # :sparkles: PYTHON :sparkles, but at the very least an exception can
        # be rasied.
        raise ValueError("__str__() was incomplete, couldn't serialize.")

class AvailableFonts(enum.Enum):
    """All fonts. This is fairly limited, but it's not horribly incomplete."""
    SERIF = 'serif'
    SANS_SERIF = 'sans-serif'
    MONOSPACE = 'monospace'

    @staticmethod
    def from_css(t: str):
        return __class__(_css_keys_for_(t, 'font-family')[0])

    def __str__(self):
        if self == AvailableFonts.SERIF:
            return 'serif'
        if self == AvailableFonts.SANS_SERIF:
            return 'sans-serif'
        if self == AvailableFonts.MONOSPACE:
            return 'monospace'
        raise ValueError("__str__() was incomplete, couldn't serialize.")

AnyStyle = Union[AvailableInlines, AvailableAligns, AvailableFonts]

SIZE_BOUNDS = (10, 18)
OFFSET_BOUNDS = (0, 5)
PARAGRAPH_CUTOFF = 30

"""Define a TextStyle by its components into an immutable named tuple."""
TextStyle = namedtuple('TextStyle', ['inlines', 'align', 'font'])
"""Same as TextStyle but with header-unique elements."""
HeaderStyle = namedtuple('HeaderStyle', ['style', 'offset_top', 'offset_bot'])
"""Same as TextStyle but with body text-unique elements."""
BodyStyle = namedtuple('HeaderStyle', ['style', 'paragraph_delim'])

"""List of all available aligns."""
AVAILABLE_ALIGNS = [
    AvailableAligns.LEFT,
    AvailableAligns.JUSTIFY,
    AvailableAligns.CENTER,
    AvailableAligns.RIGHT
]

"""List of all available inlines."""
AVAILABLE_INLINES = [
    AvailableInlines.BOLD,
    AvailableInlines.ITALIC,
    AvailableInlines.UNDERLINE
]

"""List of all available fonts."""
AVAILABLE_FONTS = [
    AvailableFonts.SERIF,
    AvailableFonts.SANS_SERIF,
    AvailableFonts.MONOSPACE
]

def text_style_from_css(css: str) -> TextStyle:
    return TextStyle(
        AvailableInlines.from_css(css),
        AvailableAligns.from_css(css),
        AvailableFonts.from_css(css)
    )

def header_style_from_css(css: str) -> HeaderStyle:
    return HeaderStyle(
        text_style_from_css(css),
        int(_css_keys_for_(css, 'margin-top')[0][:-1]),
        int(_css_keys_for_(css, 'margin-bottom')[0][:-1])
    )

def body_style_from_css(css: str) -> BodyStyle:
    return BodyStyle(
        text_style_from_css(css),
        None,
    )

def text_style_as_css(style: TextStyle) -> str:
    """Convert a namedtuple TextStyle to a CSS string."""
    css = inlines_as_css(style.inlines) \
        + align_as_css(style.align) \
        + font_as_css(style.font)

    return css

def header_style_as_css(style: HeaderStyle) -> str:
    """Convert a namedtuple HeaderStyle to a CSS string."""
    css = text_style_as_css(style.style) \
        + 'margin-top: ' + str(style.offset_top) + '%;' \
        + 'margin-bottom: ' + str(style.offset_bot) + '%;'
    return css

def body_style_as_css(style: BodyStyle) -> str:
    """Convert namedtuple BodySTyle to a CSS string."""
    css = text_style_as_css(style.style) \
        + paragraph_delim_as_css(style.paragraph_delim)
    return css

def inlines_as_css(style: list[AvailableInlines]) -> str:
    """Helper for as_css, convert a list of inlines to CSS. An empty string
    is returned if there is no valid data."""
    css = ""
    for inline in style:
        if inline == AvailableInlines.BOLD:
            css += 'font-weight: bold;'
        elif inline == AvailableInlines.ITALIC:
            css += 'font-style: italic;'
        elif inline == AvailableInlines.UNDERLINE:
            css += 'text-decoration: underline;'

    return css

def align_as_css(style: AvailableAligns) -> str:
    """Construct CSS for an AvailableAligns enum."""
    return 'text-align: ' + str(style) + ';'

def font_as_css(style: AvailableFonts) -> str:
    """Construct CSS for an AvailableFonts enum."""
    return 'font-family: ' + str(style) + ';'

def paragraph_delim_as_css(pdelim) -> str:
    if pdelim == 'newline':
        return ""
    else:
        return 'text-indent: ' + str(pdelim) + 'px;'

def random_header_style():
    return HeaderStyle(
        random_text_style(),
        random.randint(OFFSET_BOUNDS[0], OFFSET_BOUNDS[1]),
        random.randint(OFFSET_BOUNDS[0], OFFSET_BOUNDS[1])
    )

def random_body_style():
    return BodyStyle(
        random_text_style(),
        _random_paragraph_delim()
    )

def random_text_style():
    """Return a randomly picked text style. Randomness is used over a generator
    or similar scheme to ensure total coverage as its easier to multithread."""
    return TextStyle(
        random_inlines(),
        _random_align(),
        _random_font(),
    )

def random_inlines():
    """Return an array of strings from AVAILABLE_INLINES.

    algorithm from:
    https://stackoverflow.com/questions/47234958/

    Picks a random entry from the procedurally generated power set.
    Corresponds to Vec<InlineStyle> in rust"""

    n = random.randrange(1 << len(AVAILABLE_INLINES))
    result = []
    for align in AVAILABLE_INLINES:
        if n & 1:
            result.append(align)
        n >>= 1
        if not n:
            break
    return result

def _random_align():
    """Return a randomly picked alignment."""
    return AVAILABLE_ALIGNS[random.randint(0, len(AVAILABLE_ALIGNS) - 1)]

def _random_font():
    """Return a randomly picked font."""
    return AVAILABLE_FONTS[random.randint(0, len(AVAILABLE_FONTS) - 1)]

def _random_size():
    """Return a randomly picked font size."""
    return random.randint(SIZE_BOUNDS[0], SIZE_BOUNDS[1])

def _random_paragraph_delim():
    """Returns a random paragraph delimiter, wich is an integer for indent or
    the string 'newline' for a newline"""
    length = random.randint(0, PARAGRAPH_CUTOFF * 2)
    if length > PARAGRAPH_CUTOFF:
        return 'newline'
    return length

def _css_keys_for_(css: str, val: str) -> list[str]:
    """Split a string into CSS, then find the value for the given key."""
    css = re.split(r';|:|\s', css.replace(' ', ''))
    indices = [i for i, x in enumerate(css) if x == val]
    return [css[n + 1] for n in indices]
