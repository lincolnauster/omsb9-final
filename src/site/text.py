"""This module is responsible for text generation and randomness. Random text is
generated on a separate thread and stored in a queue, which doubles in capacity
every time its emptied within 4 hours (this is arbitrarily chosen) of the most
recent resize. This keeps random-text requests amortized constant."""
import random
import threading
from queue import Queue

class RandomText:
    _word_queue: Queue
    _header_queue: Queue
    _paragraph_queue: Queue
    _section_queue: Queue

    _word_queue_capacity: int
    _header_queue_capacity: int
    _paragraph_queue_capacity: int
    _section_queue_capacity: int

    _INIT_WORD_QUEUE_TARGET = 1536  # these are guesss which may need updating
    _INIT_HEADER_QUEUE_TARGET = 512 # if I ever get around to detailed profiling
    _INIT_PARAGRAPH_QUEUE_TARGET = 1536
    _INIT_SECTION_QUEUE_TARGET = 512

    def __init__(self):
        """initialize the queues with the appropriate sizes, initialize
        timestamps."""
        self._word_queue = Queue()
        self._header_queue = Queue()
        self._paragraph_queue = Queue()
        self._section_queue = Queue()

        self._word_queue_capacity = self._INIT_WORD_QUEUE_TARGET
        self._header_queue_capacity = self._INIT_HEADER_QUEUE_TARGET
        self._paragraph_queue_capacity = self._INIT_PARAGRAPH_QUEUE_TARGET
        self._section_queue_capacity = self._INIT_SECTION_QUEUE_TARGET

        self._fill_word()
        self._fill_header()
        self._fill_paragraph()
        self._fill_section()

        self._loop()

    def random_word(self):
        e = self._word_queue.get()
        self._word_queue.task_done()
        return e
    def random_header(self):
        e = self._header_queue.get()
        self._header_queue.task_done()
        return e
    def random_paragraph(self):
        e = self._paragraph_queue.get()
        self._paragraph_queue.task_done()
        return e
    def random_section(self):
        e = self._section_queue.get()
        self._section_queue.task_done()
        return e

    def random_text(self, target=None):
        count = random.randint(_TEXT_LENGTH[0], _TEXT_LENGTH[1])
        sections = []
        target_index = count // 2 + random.randint(-2, 2)
        for i in range(count):
            sections.append(dict())
            if i == target_index and target != None:
                sections[i]['target'] = True
                sections[i]['title'] = target
            else:
                sections[i]['target'] = False
                sections[i]['title']  = self.random_header()
            sections[i]['body'] = self.random_section()
        return sections

    def _loop(self):
        """For each queue, spawn a thread to perptually fill and adjust as
        necessary. This method is non-blocking."""
        def fill_word_queue():
            while True:
                self._word_queue.join() # verifies empty
                self._fill_word()
                # TODO: compare timestamp and double capacity if neceessary
        def fill_header_queue():
            while True:
                self._header_queue.join()
                self._fill_header()
        def fill_paragraph_queue():
            while True:
                self._paragraph_queue.join()
                self._fill_paragraph()
        def fill_section_queue():
            while True:
                self._section_queue.join()
                self._fill_section()

        threading.Thread(target=fill_word_queue, daemon=True).start()
        threading.Thread(target=fill_header_queue, daemon=True).start()
        threading.Thread(target=fill_paragraph_queue, daemon=True).start()
        threading.Thread(target=fill_section_queue, daemon=True).start()

    def _fill_word(self):
        """Fill all queues to their size specified in class constants, assumes
        that the quese are empty."""
        for i in range(self._word_queue_capacity):
            self._word_queue.put(_random_word())
    def _fill_header(self):
        for i in range(self._header_queue_capacity):
            self._header_queue.put(_random_header())
    def _fill_paragraph(self):
        for i in range(self._paragraph_queue_capacity):
            self._paragraph_queue.put(_random_paragraph())
    def _fill_section(self):
        for i in range(self._section_queue_capacity):
            self._section_queue.put(_random_section())

"""The allowed range of sections in generated text"""
_TEXT_LENGTH = (6, 8)
"""The allowed maximum number of paragraphs contained in a section"""
_SECTION_LENGTH_MAX = 8
"""The allowod range of sentences of contained in a paragraph"""
_PARAGRAPH_LENGTH = (3, 10)
"""The allowed maximum number of words contained in a section title"""
_HEADER_LENGTH_MAX = 6

"""Example text pool."""
_LOREM_IPSUM = """
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi nec dui eget
elit pretium fringilla eu quis massa. Pellentesque in convallis erat. Vivamus
ac lacus dolor. Suspendisse vel ipsum sit amet purus tristique aliquam a vel
nibh. Suspendisse egestas scelerisque magna, in ultricies lectus imperdiet et.
Mauris a leo ut elit vulputate hendrerit. Fusce scelerisque convallis tempus.
Nulla a bibendum dolor, vel vehicula erat.

Vivamus convallis mauris turpis, eu dignissim nibh vulputate at. Nulla in
tellus at risus dictum luctus. Today we're making a cake that can kill god.
Nulla lacus mi, interdum sed sagittis id, accumsan hound eget odio. Nam
tincidunt eros lorem, ac eleifend risus varius ut. Quisque nibh erat, luctus ut
hendrerit sit amet, hendrerit eu velit. Vestibulum sollicitudin, purus et
mattis accumsan, purus arcu consequat metus, a ultrices augue tortor a odio.
Phasellus ac dui a tortor suscipit placerat. Maecenas leo sem, pharetra sed
quam quis, tempus rutrum velit. In congue diam non erat euismod, rhoncus
posuere sapien tempor.  Donec sollicitudin sollicitudin dictum. Integer ut
magna vitae libero varius placerat. Praesent eu nunc quis mauris congue commodo
a condimentum est. Cras bibendum tristique justo. Sed id consequat massa. Duis
luctus eget sapien sit amet mollis.

Morbi dictum nibh quis massa ullamcorper, et efficitur tortor fermentum. Nullam
vitae malesuada eros. In hac habitasse platea dictumst. Vestibulum ante ipsum
primis in faucibus orci luctus et ultrices posuere cubilia curae; Donec magna
justo, aliquam ut dapibus sed, congue et ligula. Vestibulum sollicitudin nisl
nec tortor lacinia, eget pharetra metus scelerisque. Morbi vulputate arcu sed
efficitur pharetra. I am in spain without the S. Morbi tincidunt tristique
felis. Duis a dictum quam. Mauris placerat eros a hendrerit efficitur. Maecenas
maximus pulvinar velit in posuere. am tired

Nullam porttitor ultrices arcu, congue porttitor magna scelerisque in. Ut
maximus ornare lectus et sollicitudin. Vestibulum pulvinar ligula vel nibh
finibus, non sodales ipsum pellentesque. Etiam sit amet orci ac purus eleifend
lacinia et non ipsum. Praesent quam elit, euismod eget rhoncus ac, laoreet in
orci. Praesent in tempus mauris. Notice me senpai. Aliquam vel porta nisi. Duis
dolor nibh, dictum ut pharetra sit amet, posuere in sapien. Aenean dapibus,
tortor id rutrum hendrerit, metus sem lobortis felis, eu facilisis felis lorem
sit amet nisl.

Sed sit amet nibh ullamcorper, rhoncus mauris vel, tempor dui. Vivamus
fermentum hendrerit metus, eget vestibulum tortor. Mauris quis dui tempus,
luctus quam vitae, feugiat velit. Donec facilisis lectus nec purus suscipit
vulputate. Aenean faucibus ante et euismod imperdiet. Class aptent taciti
sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
Vestibulum sit amet diam sapien. Cras odio libero, laoreet non risus et, semper
luctus lectus. Ut gravida ante vitae felis vulputate ornare. In feugiat, sem
sit amet mattis feugiat, est tellus tempor tellus, non iaculis justo enim vitae
mauris. Suspendisse porta nibh varius ligula blandit vulputate vel vitae felis.
Donec ac libero suscipit, imperdiet leo eget, elementum ex. Donec laoreet elit
orci, suscipit congue lacus porta in. Quisque condimentum nunc in dui
vestibulum rutrum. Quisque at venenatis nunc. Integer id mi ut nisi viverra
auctor.

Mauris aliquam augue a diam rhoncus, ac congue lectus sagittis. Praesent est
ipsum, malesuada at cursus fringilla, semper in nulla. Morbi in tincidunt augue.
Integer vitae diam velit. Nunc non venenatis dui. Nulla facilisi. Mauris
consectetur consequat fermentum. Maecenas purus felis, mollis eu posuere nec,
bibendum porttitor metus. Mauris quis feugiat leo, sed sollicitudin dui. Aliquam
auctor iaculis accumsan. Ut at ligula molestie ipsum mollis porta. Vestibulum
elit nulla, fringilla a vulputate et, scelerisque at lacus. Cras in quam at orci
facilisis faucibus. Cras ut feugiat urna. Proin finibus dui et elit egestas
queueing maximus. UwU.

Pellentesque a nisi dictum, consectetur sapien sed, pellentesque nibh. Integer
non massa molestie, viverra enim nec, ultrices orci. Aliquam in lobortis augue.
Integer id lorem lorem. Cras in sem eu est aliquet hendrerit. Duis vel turpis
malesuada, commodo felis sit amet, lacinia leo. Aliquam nulla nunc, viverra eget
efficitur eu, consectetur nec augue. Quisque egestas arcu a augue consequat
porttitor. Sonic the hedgehog says: gotta go fast. Vestibulum suscipit nisi ut
urna hendrerit lacinia. Cras sollicitudin gravida lacus eu egestas. Curabitur
maximus purus quis leo pulvinar placerat. Praesent in eros nec lorem ornare
varius.
"""

_LOREM_IPSUM_WORDS = _LOREM_IPSUM.replace('.', '').split()
_LOREM_IPSUM_WORDS = list(map(lambda x: x.capitalize(), _LOREM_IPSUM_WORDS))
_LOREM_IPSUM_SENTENCES = _LOREM_IPSUM.split('.')

def _random_word() -> str:
    """Return a random word from the given example text."""
    return _LOREM_IPSUM_WORDS[random.randint(0, len(_LOREM_IPSUM_WORDS) - 1)]

def _random_header() -> str:
    """Return a randomly generated header containing random words of a length
    (roughly) normally distributed but below the private global constant."""
    out = ""
    for i in range(random.randint(1, _HEADER_LENGTH_MAX)):
        out += _random_word() + ' '
        if random.randint(0, 1):
            break;
    return out

def _random_paragraph() -> str:
    """Return a random paragraph (with the length established in private
    constants) from the sample text."""
    count = random.randint(_PARAGRAPH_LENGTH[0], _PARAGRAPH_LENGTH[1])
    return '. '.join(random.sample(_LOREM_IPSUM_SENTENCES, k=count))

def _random_section() -> list[str]:
    """Return a randomly generated section containing random paragraphs of a
    length (roughly) normally distributed but below the private global
    constant."""
    out = []
    for i in range(random.randint(1, _SECTION_LENGTH_MAX)):
        out.append(_random_paragraph())
    return out

def _random_text(target=None):
    """Generate a list of random sections, where each section is a dictionary
    with keys `target`, `title`, and `body`. `target` is a boolean value, which
    is true when section.header is equal to the target passed. `title` is a
    string, and `body` is a list of string paragraphs."""
    sections = []
    count = random.randint(_TEXT_LENGTH[0], _TEXT_LENGTH[1])
    target_index = count / 2
    for i in range(count):
        sections.append(dict())
        if i == target_index and target != None:
            sections[i]['target'] = True
            sections[i]['title'] = target
        else:
            sections[i]['target'] = False
            sections[i]['title'] = _random_header()
        sections[i]['body'] = _random_section()
    return sections
