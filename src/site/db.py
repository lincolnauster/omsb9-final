"""The database module provides a single connection to the database (through a
private class _Conn, and classes to generate requests for that connection."""
import queue
import socket
import threading
import os

import fmt

class _Conn:
    _queue: queue.Queue
    _db_conn: socket.socket
    # Only connect when the connection is needed: as this class is instantiated
    # on module import, it shouldn't take system resources to connect to the IPC
    # socket if the connection is unnecessary.
    _connected: bool

    def __init__(self):
        self._connected = False

        self._queue = queue.Queue()
        threading.Thread(target=self._work, daemon=True).start()

    def __del__(self):
        self._db_conn.close()

    def add(self, item):
        self._queue.put(item)

    def _work(self):
        """Send all strings in the queue."""
        while True:
            item = self._queue.get() # blocks until available

            if not self._connected:
                self._connect()

            self._db_conn.send(item)
            self._db_conn.send(bytes('\n', 'utf-8'))

    def _connect(self):
        try:
            self._db_conn = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
            self._db_conn.connect('/tmp/msb-final-ipc-socket')
        except ConnectionRefusedError:
            print("Connection was refused. Is the server running?")
            raise
        except FileNotFoundError:
            print("Socket wasn't found. Is the server running?")
            raise

"""The conn variable should be used to interact with the database. This is used
over having each client instantiate a connection themself to reduce any globally
mutable state."""
conn = _Conn()

class PutContrastCoefficient:
    """Mirrors the PutRequest enum's ContrastCoefficients variant in the rust
    db."""
    a: fmt.AnyStyle
    b: fmt.AnyStyle
    v: float

    def __init__(self, a: fmt.AnyStyle, b: fmt.AnyStyle, v):
        assert(type(a) == type(b))

        self.a = a
        self.b = b
        self.v = v

    def __str__(self):
        """Serialize for IPC transfer"""
        res = 'PC'
        if type(self.a) == list:
            # assume that the list is of inlines
            res += self._serialize_inlines()
        elif type(self.a) == fmt.AvailableAligns:
            res += self._serialize_aligns()
        elif type(self.a) == fmt.AvailableFonts:
            res += self._serialize_fonts()
        else:
            raise NotImplementedError(str(type(self.a)))
        return res

    def _serialize_aligns(self) -> str:
        assert(isinstance(self.a, fmt.AvailableAligns))
        res = 'A'
        res += str(self.a) + ' A' + str(self.b) + ' '
        res += str(self.v)
        return res

    def _serialize_fonts(self) -> str:
        assert(isinstance(self.a, fmt.AvailableFonts))
        res = 'F'
        res += str(self.a) + ' F' + str(self.b) + ' '
        res += str(self.v)
        return res

    def _serialize_inlines(self) -> str:
        assert(isinstance(self.a, list))
        res = 'I'
        for inline in self.a:
            res += str(inline) + ' '
        res += ': I'
        for inline in self.b:
            res += str(inline) + ' '
        res += ': '
        res += str(self.v)
        return res


class PutTimeStyleRecord:
    _id: int
    _time: float
    _body_style: fmt.BodyStyle
    _header_style: fmt.HeaderStyle

    def __init__(self, id: int, time: float, bs: fmt.BodyStyle, hs: fmt.HeaderStyle):
        self._id = id
        self._time = time
        self._body_style = bs
        self._header_style = hs

    def __str__(self):
        res = 'PU'
        res += hex(self._id)[2:] # cut out the 0x
        res += ' '
        res += str(self._time)
        res += ' '
        res += self._serialize_header()
        res += ' '
        res += self._serialize_body()
        return res

    def _serialize_header(self) -> str:
        res = ''
        for inline in self._header_style.style.inlines:
            res += str(inline)
            res += ' '
        res += ' : '
        res += str(self._header_style.style.align)
        res += ' '
        res += str(self._header_style.style.font)
        res += ' '
        res += str(self._header_style.offset_top)
        res += ' '
        res += str(self._header_style.offset_bot)
        return res

    def _serialize_body(self) -> str:
        res = ''
        for inline in self._header_style.style.inlines:
            res += str(inline)
            res += ' '
        res += ' :'
        res += ' '
        res += str(self._header_style.style.align)
        res += ' '
        res += str(self._header_style.style.font)
        return res
