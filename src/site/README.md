# Final Project Frontend and Server
This directory contains the Python Flask (subject to change) sources for the
frontend of the website. This README contains implementation information as well
as an overview of the UI.

## Implementation
Leaning towards Python and Flask, communicating through Python's socket
implementation with the database. This *should* keep everything as small and
easy as possible on the backend, while the frontend can just be hacked together
in HTML and the smallest possible amount of JS.

## Design and UI
High-level functions of the website are three-fold:
- Collect contrast coefficient data
- Collect time-contrast data pairs
- Aggregation of information for presentation

These may map 1-to-1 with pages on the website, with the following URLs:
- https://\<domain-name\>/contrast\_coefficients
- https://\<domain-name\>/time\_contrast
- https://\<domain-name\>/aggregation

Navigation between these three functions is central to the functionality of the
website, so a navigation bar present on all pages would be beneficial. As there
is no 'default' of these three functions, an index page that provides background
information is required.

### Index
Background information about the project, as well as link to sources and
submissions.

### Contrast Coefficients
This page exists to compare text styles and determine ratio distance of nominal
pairs. On load, the server generates a random pairing and returns the page with
the appropriate specifications. Distance is then rated by the user on an
interval 1-10 scale. Upon submittal, the result is returned to and logged by the
server, and new contents are requested.

### Time Contrast
This page exists to time navigation to a given heading. On load, the server
generates text and heading, which are appropriately formatted and sent as a
response. The webpage will then time the duration between load and clicking on
the header, which, accompanied by a fingerprint or other ID, is returned to and
logged by the server. New contents are then requested.

### Aggregation
This displays summary statistics of the datasets, and determines whether the
correlation coefficient between navigability and contrast is statistically
significant.

Below that, any extra statistics that can easily be made available are. This
includes header/body style pairings that result in the most contrast, average
reading speed offset of an arbitrary pair, etc. These are only including here as
most of them are _essentially_ glorified p-hacking due to their dependence on
random change, but they're interesting enough that they should be included.

These statistics are calculated server-side every 6 hours: a snapshot of the
in-memory database is exported to a file, that snapshot's statistics are
calculated, and a cache is updated. This cache is what is served on the
aggregation page.

## TODOs
- [X] Templates (front-end mockups in HTML)
- [X] Set up flask, `venv`
- [ ] everything else :sparkles:
