# OMSB9 Final db

The database contains all records of activity on the hosted website, accessible
and modifiable via UNIX Socket IPC. This is a very narrow database, holding
_only_ what is pre-programmed as necessary for analysis. It is **not** scalable
to a general purpose database, and is as such for convenience. Review
ARCHITECTURE.md for implementation details.

## Features
- [X] Manipulate database in-memory
- [ ] IPC Control
    - [ ] design file format owo
    - [ ] good failing for IPC read errs
- [ ] Support saving/loading to/from disk (for backups)
