//! Run required statistical tests over data
use std::cmp::Eq;
use std::collections::HashMap;
use std::hash::Hash;

/// Simple mean
pub fn avg(l: &mut impl std::iter::Iterator<Item = f32>) -> f32 {
    let mut sum = 0.0;
    let mut len = 0;
    for n in l {
        sum += n;
        len += 1;
    }

    sum / len as f32
}

/// Sample standard deviation (sum of suared differences / dof)
pub fn stdev(
    l: &mut (impl std::iter::Iterator<Item = f32> + Clone)
) -> Option<f32> {
    let x_bar = avg(&mut l.clone());
    let len = l.clone().collect::<Vec<f32>>().len();

    match len {
        0..=1 => None,
        len => Some(
            l.map(|x| (x - x_bar).powf(2.0)).sum::<f32>() / len as f32
        ),
    }
}

/// Given a hashmap of ((T, T), Vec<f32>), compute the average for each pairing
/// of T's.
pub fn average_for_pairs<T: Clone + Eq + Hash>(
    map: &HashMap<(T, T), Vec<f32>>,
) -> HashMap<(T, T), f32> {
    let mut result: HashMap<(T, T), f32> = HashMap::new();
    let len = counts_for_pairs(map);

    for ((a, b), v) in map.iter() {
        if result.get(&(a.clone(), b.clone())) != None {
            continue;
        }

        let len = match len.get(&(a.clone(), b.clone())) {
            Some(v) => v,
            None => len.get(&(b.clone(), a.clone())).unwrap(),
        };

        let mut sum = v.iter().sum::<f32>();
        if a != b {
            sum += match map.get(&(b.clone(), a.clone())) {
                Some(v) => v.iter().sum::<f32>(),
                None => 0.0,
            };
        }

        let avg = sum / (*len as f32);

        result.insert((a.clone(), b.clone()), avg);
    }

    result
}

/// Given a hashmap of ((T, T), Vec<f32>), compute frequency data.
pub fn counts_for_pairs<T: Clone + Eq + Hash, U>(
    map: &HashMap<(T, T), Vec<U>>,
) -> HashMap<(T, T), usize> {
    let mut result = HashMap::new();

    for ((a, b), v) in map.iter() {
        if result.get(&(a.clone(), b.clone())) != None {
            continue;
        }

        let mut count = v.len();
        if a != b {
            count += match map.get(&(b.clone(), a.clone())) {
                Some(v) => v.len(),
                None => 0,
            };
        }

        result.insert((a.clone(), b.clone()), count);
    }

    result
}

/// Pearson moment correlation coefficient. Returns a 3-tuple of (correlation
/// coefficient, *n*).
pub fn pmcc(data: Vec<(f32, f32)>) -> (f32, usize) {
    let x = data.iter().map(|(x, _)| *x);
    let y = data.iter().map(|(_, y)| *y);

    let x_bar = avg(&mut x.clone());
    let y_bar = avg(&mut y.clone());

    let covar: f32 = data.iter().map(|(x, y)| (x - x_bar) * (y - y_bar)).sum();
    let x_var: f32 = x.clone().map(|x| (x - x_bar).powf(2.0)).sum::<f32>().sqrt();
    let y_var: f32 = y.clone().map(|y| (y - y_bar).powf(2.0)).sum::<f32>().sqrt();

    let correlation_coefficient = covar / (x_var * y_var);

    (correlation_coefficient, data.len())
}

#[cfg(test)]
mod tests {
    use super::*;
    use float_cmp::approx_eq;

    #[test]
    fn average_for_simple_pairs() {
        let mut test: HashMap<(u32, u32), Vec<f32>> = HashMap::new();
        test.insert((1, 1), vec![0.0, 1.0]);
        test.insert((1, 2), vec![0.0, 1.0]);
        let test = average_for_pairs(&test);

        let mut expected: HashMap<(u32, u32), f32> = HashMap::new();
        expected.insert((1, 1), 0.5);
        expected.insert((1, 2), 0.5);

        let one_one = test.get(&(1, 1)).unwrap();
        let one_two = test.get(&(1, 2)).unwrap();
        assert!(approx_eq!(f32, *one_one, 0.5));
        assert!(approx_eq!(f32, *one_two, 0.5));
    }

    #[test]
    fn average_for_overlapping_pairs() {
        let mut test: HashMap<(u32, u32), Vec<f32>> = HashMap::new();
        test.insert((2, 1), vec![0.0, 1.0]);
        test.insert((1, 2), vec![0.0, 1.0]);
        let test = average_for_pairs(&test);

        let mut expected: HashMap<(u32, u32), f32> = HashMap::new();
        expected.insert((2, 1), 0.5);

        let one_two = test.get(&(2, 1)).unwrap();
        println!("{}\n", *one_two);
        assert!(approx_eq!(f32, *one_two, 0.5));
    }

    #[test]
    fn counts_for_simple_pairs() {
        let mut test: HashMap<(u32, u32), Vec<f32>> = HashMap::new();
        test.insert((1, 1), vec![0.0, 1.0]);
        test.insert((1, 2), vec![0.0, 1.0]);
        let test = counts_for_pairs(&test);

        assert_eq!(*(test.get(&(1, 1)).unwrap()), 2 as usize);
    }

    #[test]
    fn counts_for_overlapping_pairs() {
        let mut test: HashMap<(u32, u32), Vec<f32>> = HashMap::new();
        test.insert((2, 1), vec![0.0, 1.0]);
        test.insert((1, 2), vec![0.0, 1.0]);
        let test = counts_for_pairs(&test);

        assert_eq!(*(test.get(&(2, 1)).unwrap()), 4 as usize);
    }
}
