use std::env;
use std::fs::File;

use msbfinal_db::communication::IPCServer;
use msbfinal_db::parse::Interface;
use msbfinal_db::Database;

fn main() {
    let path = env::args().nth(1);
    let db = match path {
        None => Database::new(),
        Some(s) => Database::from(File::open(s).expect("Couldn't read file"))
            .expect("Couldn't parse file."),
    };

    let interface = Interface::from(db);
    let server =
        IPCServer::new(Box::new(Interface::handle), interface).unwrap();

    server.main_loop();
}
