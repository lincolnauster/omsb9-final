//! Responsible for parsing requests (obtained from communication).
use crate::*;

/// Wrap a Database and allow IPC request handling. Could provide logging if I
/// have time and that's helpful.
pub struct Interface(Database);

/// The result type when parsing an IPC string.
pub type Result<T> = std::result::Result<T, ParseError>;

/// The error type when parsing an IPC string
#[derive(Debug, Eq, PartialEq)]
pub enum ParseError {
    /// Expected information wasn't seen.
    UnexpectedEnd,
    /// A character that wasn't understood was seen in the prelude, i.e.,
    /// putting A instead of G or P in the first position.
    MisunderstoodPrelude,
    /// An argument is expected but either just whitespace or not present
    ExpectedArgument,
    /// An align was expected, String was recieved.
    ExpectedAlign(String),
    /// A font was expected, String was recieved.
    ExpectedFont(String),
    /// An inline style was expected, String was recieved.
    ExpectedInlineStyle(String),
    /// An integer was expected, String was recieved.
    ExpectedInt(String),
    /// A float was expected, String was recieved.
    ExpectedFloat(String),
    /// A paragraph delimiter was expected, String was recieved.
    ExpectedParDelim(String),
    /// A style type indicator was expceted, char was recieved.
    ExpectedStyleTypeIndicator(char),
}

impl<'a> Interface {
    /// get request
    pub const PRELUDE_GET_INDICATOR: char = 'G';
    /// put request
    pub const PRELUDE_PUT_INDICATOR: char = 'P';
    /// get/put user data indicator
    pub const PRELUDE_UD_INDICATOR: char = 'U';
    /// get/put all user data indicator
    pub const PRELUDE_AD_INDICATOR: char = 'A';
    /// get/put contrast coefficients indicator
    pub const PRELUDE_CC_INDICATOR: char = 'C';
    /// get request contrast coefficient means
    pub const PRELUDE_CCM_INDICATOR: char = 'M';
    /// get request contrast coefficient frequencies
    pub const PRELUDE_CCF_INDICATOR: char = 'F';
    /// get request navigability people count
    pub const PRELUDE_NPC_INDICATOR: char = 'N';
    /// write to disk
    pub const PRELUDE_WRITE_INDICATOR: char = 'W';
    /// Get contrast between 2 *s*tyles
    pub const PRELUDE_CONTRAST_INDICATOR: char = 'S';
    /// Get (contrast, time) pairs
    pub const PRELUDE_TIME_CONTRAST_INDICATOR: char = 'T';
    /// Get PMCC
    pub const PRELUDE_PMCC_INDICATOR: char = 'P';
    /// Get CSV repr
    pub const PRELUDE_CSV_INDICATOR: char = 'V';


    /// When expecting a style, this character indicates that the style is a set
    /// of inlines.
    pub const STYLE_INLINE_INDICATOR: char = 'I';
    /// When expecting a style, this character indicates that the style is an
    /// align.
    pub const STYLE_ALIGN_INDICATOR: char = 'A';
    /// When expecting a style, this character indicates that the style is a
    /// font.
    pub const STYLE_FONT_INDICATOR: char = 'F';
    /// When expecting a style, this character indicates that the style is a
    /// size.
    pub const STYLE_SIZE_INDICATOR: char = 'S';

    /// Initialize an interfac from a Database
    pub fn from(db: Database) -> Self {
        Interface(db)
    }

    /// Handle an IPC request - decodes, processes, and returns a result if
    /// required.
    pub fn handle(
        inter: &mut Self,
        request: String,
    ) -> Result<Option<GetRequestAns>> {
        eprintln!("{}", request);
        let rq = match Self::parse(request) {
            Ok(rq) => rq,
            Err(e) => {
                eprintln!("Err: {:?}", e);
                return Err(e);
            }
        };
        println!("{:?}", rq);
        Ok(inter.0.process(rq))
    }

    /// Decode a string (probably sent via IPC to a request).
    ///
    /// Request sent determine their type with a preamble, determing, in order,
    /// \[G\]et or \[P\]ut requests, if a put request, \[U\]serData or
    /// \[C\]ontrastCoefficients, and if a get request, \[U\]serData,
    /// \[A\]llUserData, \[C\]ontrastCoefficients.
    ///
    /// For example, the preamble to specify a get request concering all user
    /// data would read `GA`.
    ///
    /// The rest is ✨ TODO ✨
    pub fn parse(response: String) -> Result<Request> {
        let mut chars = response.chars();

        Ok(match chars.next() {
            Some(Self::PRELUDE_GET_INDICATOR) => Self::parse_get(&mut chars)?,
            Some(Self::PRELUDE_PUT_INDICATOR) => Self::parse_put(&mut chars)?,
            Some(Self::PRELUDE_WRITE_INDICATOR) => Request::WriteRequest,
            None => {
                return Err(ParseError::UnexpectedEnd);
            }
            _ => {
                return Err(ParseError::MisunderstoodPrelude);
            }
        })
    }

    fn parse_get(chars: &mut std::str::Chars) -> Result<Request> {
        Ok(Request::GetRequest(match chars.next() {
            Some(Self::PRELUDE_UD_INDICATOR) => {
                GetRequest::UserData(next_hex(chars)?)
            }
            Some(Self::PRELUDE_AD_INDICATOR) => GetRequest::AllUserData,
            Some(Self::PRELUDE_CC_INDICATOR) => {
                GetRequest::ContrastCoefficients(
                    next_style(chars)?,
                    next_style(chars)?,
                )
            }
            Some(Self::PRELUDE_CCM_INDICATOR) => {
                GetRequest::ContrastCoefficientsMeans
            }
            Some(Self::PRELUDE_CCF_INDICATOR) => {
                GetRequest::ContrastCoefficientsCounts
            }
            Some(Self::PRELUDE_NPC_INDICATOR) => {
                GetRequest::NavigabilityPeopleCount
            }
            Some(Self::PRELUDE_CONTRAST_INDICATOR) => GetRequest::Contrast(
                next_text_style(chars)?,
                next_text_style(chars)?,
            ),
            Some(Self::PRELUDE_TIME_CONTRAST_INDICATOR) => GetRequest::ContrastTimePairs,
            Some(Self::PRELUDE_PMCC_INDICATOR) => GetRequest::PMCC,
            Some(Self::PRELUDE_CSV_INDICATOR) => GetRequest::CSV,
            None => {
                return Err(ParseError::UnexpectedEnd);
            }
            _ => {
                return Err(ParseError::MisunderstoodPrelude);
            }
        }))
    }

    fn parse_put(chars: &mut std::str::Chars) -> Result<Request> {
        Ok(Request::PutRequest(match chars.next() {
            Some(Self::PRELUDE_UD_INDICATOR) => PutRequest::UserData(
                next_hex(chars)?,
                (
                    next_float(chars)?,
                    (next_header_style(chars)?, next_body_style(chars)?),
                ),
            ),
            Some(Self::PRELUDE_CC_INDICATOR) => {
                PutRequest::ContrastCoefficients(
                    (next_style(chars)?, next_style(chars)?),
                    next_float(chars)?,
                )
            }
            None => {
                return Err(ParseError::UnexpectedEnd);
            }
            _ => {
                return Err(ParseError::MisunderstoodPrelude);
            }
        }))
    }
}

// Parse the next element of the iterator as a hexadecimal number, ignore
// whitespace
fn next_hex(chars: &mut std::str::Chars) -> Result<u64> {
    let mut x: u64 = 0;
    let mut start = true;
    while let Some(c) = chars.next() {
        if c.is_whitespace() {
            break;
        }

        if let Some(c) = c.to_digit(16) {
            x *= 16;
            x += c as u64;
            start = false;
        } else {
            break;
        }
    }

    if !start {
        return Ok(x);
    } else {
        return Err(ParseError::ExpectedArgument);
    }
}

fn next_int(chars: &mut std::str::Chars) -> Result<i32> {
    let mut active = String::new();
    until_whitespace(chars, &mut active);
    Ok(match active.parse::<i32>() {
        Ok(i) => i,
        Err(_) => {
            return Err(ParseError::ExpectedInt(active));
        }
    })
}

fn next_float(chars: &mut std::str::Chars) -> Result<f32> {
    let mut active = String::new();
    until_whitespace(chars, &mut active);
    Ok(match active.parse::<f32>() {
        Ok(i) => i,
        Err(_) => {
            return Err(ParseError::ExpectedFloat(active));
        }
    })
}

// Read a style: decide between inlines (i), align (a), font (f), size (s), or
// pargraph delimiter (p)
fn next_style(chars: &mut std::str::Chars) -> Result<Style> {
    match chars.next() {
        None => Err(ParseError::UnexpectedEnd),
        Some(Interface::STYLE_INLINE_INDICATOR) => {
            Ok(Style::Inlines(next_inline_styles(chars)?))
        }
        Some(Interface::STYLE_ALIGN_INDICATOR) => {
            Ok(Style::Align(next_align(chars)?))
        }
        Some(Interface::STYLE_FONT_INDICATOR) => {
            Ok(Style::Font(next_font(chars)?))
        }
        Some(c) => Err(ParseError::ExpectedStyleTypeIndicator(c)),
    }
}

fn next_text_style(chars: &mut std::str::Chars) -> Result<TextStyle> {
    Ok(TextStyle {
        inlines: next_inline_styles(chars)?,
        align: next_align(chars)?,
        font: next_font(chars)?,
    })
}

// Read a textstyle, look for (in order): inline styles : align : font : size
fn next_body_style(chars: &mut std::str::Chars) -> Result<BodyStyle> {
    Ok(next_text_style(chars)?)
}

fn next_header_style(chars: &mut std::str::Chars) -> Result<HeaderStyle> {
    Ok(HeaderStyle {
        style: next_text_style(chars)?,
        offset_top: next_int(chars)?,
        offset_bot: next_int(chars)?,
    })
}

// Read inline styles from a chars iter
fn next_inline_styles(chars: &mut std::str::Chars) -> Result<Inlines> {
    let mut i_styles: Inlines = Vec::new();
    let mut active = String::new();
    loop {
        until_whitespace(chars, &mut active);
        match &active[..] {
            ":" => {
                break;
            }
            "bold" => i_styles.push(InlineStyle::Bold),
            "italic" => i_styles.push(InlineStyle::Italic),
            "underline" => i_styles.push(InlineStyle::Underline),
            "" => {
                continue;
            }
            _ => {
                return Err(ParseError::ExpectedInlineStyle(active));
            }
        };
        active = String::new();
    }

    Ok(i_styles)
}

fn next_font(chars: &mut std::str::Chars) -> Result<Font> {
    let mut active = String::new();

    until_whitespace(chars, &mut active);
    Ok(match &active[..] {
        "serif" => Font::Serif,
        "sans-serif" => Font::SansSerif,
        "monospace" => Font::Mono,
        _ => {
            return Err(ParseError::ExpectedFont(active));
        }
    })
}

// Read an align from a chars iter
fn next_align(chars: &mut std::str::Chars) -> Result<Align> {
    let mut active = String::new();
    until_whitespace(chars, &mut active);
    Ok(match &active[..] {
        "left" => Align::Left,
        "justify" => Align::Justified,
        "center" => Align::Center,
        "right" => Align::Right,
        _ => {
            return Err(ParseError::ExpectedAlign(active));
        }
    })
}

// Read a chars iterator until whitespace is seen to a buffer
fn until_whitespace(chars: &mut std::str::Chars, buffer: &mut String) {
    for c in chars {
        if c.is_whitespace() {
            break;
        } else {
            buffer.push(c);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_gets() {
        assert_eq!(
            Interface::parse("GA".to_string()),
            Ok(Request::GetRequest(GetRequest::AllUserData))
        );
        assert_eq!(
            Interface::parse("GU1".to_string()),
            Ok(Request::GetRequest(GetRequest::UserData(1)))
        );
    }
}
