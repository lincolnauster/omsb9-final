//! The file module encapsulates file I/O for easy use in main module.
use std::fs;
use std::path::Path;

/// The file structure maintains a file name and contents
pub struct BinFile {
    suffix: String,
    contents: Vec<u8>,
}

impl BinFile {
    const PREFIX: &'static str = "/var/db/db";

    /// Create a database file from serialized data.
    pub fn from_contents(contents: Vec<u8>) -> std::io::Result<Self> {
        let mut suffix: u8 = 0;
        while Path::new(&(Self::PREFIX.to_owned() + &suffix.to_string()))
            .exists()
        {
            suffix += 1;
        }

        fs::File::create(Self::PREFIX.to_owned() + &suffix.to_string())?;

        Ok(BinFile {
            suffix: suffix.to_string(),
            contents: contents,
        })
    }

    /// Write out the data to the file
    pub fn write(self) -> std::io::Result<()> {
        fs::write(self.path(), self.contents)
    }

    /// Retrieve the path
    pub fn path(&self) -> String {
        Self::PREFIX.to_owned() + &self.suffix
    }
}
