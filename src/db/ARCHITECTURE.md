# db Architecture

The data stored is of two categories: contrast coefficients and user data.

## Contrast Coefficients
Contrast coefficients are used to quantify the contrast of nominal values, i.e.,
between **_bold and italic text_**, and _italic text_. They are a floating point
value between 0 and 1, and are gathered as a collection of individuals' contrast
ratings on a scale of 1 to 10, gathered via the frontend. Data stored here is a
list of all of these ratings for all the combinations. For speed, this is
represented as a `HashMap<TextStyle, Hashmap<TextStyle, Vec<f32>>>`, which
emulates a 2-dimensional array but with constant time access to any arbitrary
element by value, with minimal cache hit-rate compromise due to linear probing.
The contained value, a `Vec<f32>`, is a list of all ratings this combination has
received. It may be empty, and handling empty values must be dealt with by the
aggregation code.

## User Data
User data maps a unique user identifier (obtained either via browser fingerprint
or user-determined string, TODO) to a vector of `(time: f32, (text: TextStyle,
header: HeaderStyle))` tuples (or structures, TODO). This records everything and
allows as much flexibility as possible in aggregation. The `Traits` type is an
abstraction containing the traits that define the style of the text.

## Perspective
The values stored here allow for statistical tests to precisely determine
correlation between navigability and contrast, tests that must be performed by
external code.
